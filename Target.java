public interface Target {
    public void receive(Projectiles prj);
    public double getPosition();
}