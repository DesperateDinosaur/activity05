public class Player {
    //Fields
    private String name;
    private double position;
    private Projectiles heldProjectile;

    //Constructor
    public Player(){
        this.name = "Gabriel";
        this.position = 20.0;
    }

    //Getter methods
    public String getName(){
        return this.name;
    }

    public double getPosition(){
        return this.position;
    }

    //Give projectile to player
    public boolean giveProjectile(Projectiles projectile){
        //If not already holding projectile, give player projectile
        if (this.heldProjectile == null){
            this.heldProjectile = projectile;
            return true;
        }
        //If already holding projectile, do not give player projectile
        else{
            return false;
        }
    }

    //Toss projectile to target
    public boolean toss(Target target){
        if (this.heldProjectile == null){
            return false;
        }
        else{
            this.heldProjectile.beTossed(this, target);
            return true;
        }
    }
}